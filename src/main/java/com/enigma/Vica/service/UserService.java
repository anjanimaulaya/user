package com.enigma.Vica.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enigma.Vica.entity.User;
import com.enigma.Vica.repository.UserRepository;
import com.enigma.Vica.responses.UserDTO;

@Service
public class UserService {
	
	 @Autowired
	 UserRepository userRepository;
	 
	 public List<User> getAllUser(){
		 List<User> listUser = userRepository.findAll();
		 return listUser;
	 }

	 public Optional<User> getUser(String id) {
		 Optional<User> user = userRepository.findById(id);
		 return user;
	 }
	 
	 public UserDTO saveUser(UserDTO dto) {
		 
		 User user = new User(dto.getFirstName(), dto.getLastName(), dto.getEmailAddress(), dto.getPhoneNumber(), dto.getPassword());
		 userRepository.save(user);
		 ModelMapper modelMapper = new ModelMapper();
		 UserDTO userDto = modelMapper.map(user, UserDTO.class);
		 return userDto;
	 }
	 
	 public User updateUser(User request) {
		 Optional<User> user = userRepository.findById(request.getId());	 
		 user.get().setFirstName(request.getFirstName());
		 user.get().setLastName(request.getLastName());
		 user.get().setEmailAddress(request.getEmailAddress());
		 user.get().setPhoneNumber(request.getPhoneNumber());
		 user.get().setPassword(request.getPassword());
		 userRepository.save(user.get());	 
		 return request;
		 
	 }
	 
	 public void deleteUser(String id) {
		 userRepository.deleteById(id);
	 }
	 
	 
}
