package com.enigma.Vica.responses;

public class MahasiswaDTO {
	
	private String id;
	private String nama;
	private String alamat;
	
	public MahasiswaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MahasiswaDTO(String id, String nama, String alamat) {
		super();
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	
	
	

}
