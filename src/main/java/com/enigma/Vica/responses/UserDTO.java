package com.enigma.Vica.responses;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO {
	
	@NotEmpty(message = "First name is required, can't be empty")
	@Size(min = 3, message = "First name must be longer than 3 characters")
	private String firstName;
	
	private String lastName;
	
	@NotEmpty(message = "Email Address is required, can't be empty")
	@Email(regexp = ".+@{1}.+\\..+", message = "invalid email address")
	private String emailAddress;
	
	@NotEmpty(message = "Phone Number is required, can't be empty")
	@Pattern(regexp = "0\\d{5,}", message = "invalid phone number")
	private String phoneNumber;
	
	@NotEmpty(message = "Password is required, can't be empty")
	@Pattern(regexp = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}", message = "invalid password")
	private String password;
	
	public UserDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDTO(
			@NotEmpty(message = "First name is required, can't be empty") @Size(min = 3, message = "First name must be longer than 3 characters") String firstName,
			String lastName,
			@NotEmpty(message = "Email Address is required, can't be empty") @Email(regexp = ".+@{1}.+\\..+", message = "invalid email address") String emailAddress,
			@NotEmpty(message = "Phone Number is required, can't be empty") @Pattern(regexp = "0\\d{5,}", message = "invalid phone number") String phoneNumber,
			@NotEmpty(message = "Password is required, can't be empty") @Pattern(regexp = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}", message = "invalid password") String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.password = password;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	
}
