package com.enigma.Vica.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enigma.Vica.entity.User;
import com.enigma.Vica.responses.UserDTO;
import com.enigma.Vica.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
    UserService userService;
	
	@GetMapping("")
	public List<User> getAllUser(){
		return userService.getAllUser();
	}
	
	@GetMapping("/{id}")
	public Optional<User> getUser(@PathVariable String id) {
		return userService.getUser(id);
	}

    @PostMapping("")
    public UserDTO saveUser(@Valid @RequestBody UserDTO user){
        return userService.saveUser(user);
    }
    
    @PutMapping("")
    public User updateUser(@Valid @RequestBody User user) {
    	return userService.updateUser(user);
    }
    
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable String id) {
    	userService.deleteUser(id);
    }
    
    
    
    

}
