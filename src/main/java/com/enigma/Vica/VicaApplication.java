package com.enigma.Vica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VicaApplication.class, args);
	}

}
