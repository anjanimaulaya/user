package com.enigma.Vica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enigma.Vica.entity.User;

public interface UserRepository extends JpaRepository<User, String>{
	public Optional<User> findById(String id);

}
