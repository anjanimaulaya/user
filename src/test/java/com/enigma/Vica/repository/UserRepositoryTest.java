package com.enigma.Vica.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.enigma.Vica.entity.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
    public void testShouldSaveUser() {
		User user = new User();
		user.setFirstName("Anjani");
		user.setLastName("Maulaya");
		user.setEmailAddress("anjani.maulaya@Gmail.com");
		user.setPhoneNumber("082213456789");
		user.setPassword("Anjani12345");
		
        User savedUser = userRepository.save(user);
 
        assertThat(savedUser.getId()).isNotNull();
        assertThat(entityManager.find(User.class, savedUser.getId())).isNotNull();
    }
	
	@Test
    public void testShouldFindByIdWhenUserExists() {		
		User user = new User("Anjani", "Maulaya", "anjani.maulaya@gmail.com", "082218297389", "Anjani123");
        user = entityManager.persistAndFlush(user);
        
        assertThat(userRepository.findById(user.getId()).get()).isEqualTo(user);
    }
 
    @Test
    public void testFindByIdShouldReturnEmptyWhenUserNotFound() {
        String nonExistingID  = "ff80818172e096e50172e098c0e00000";
         
        assertThat(userRepository.findById(nonExistingID)).isEqualTo(Optional.empty());
    }
    
    @Test
    public void testShoulFindAllUsers() {
    	User user = new User("Anjani", "Maulaya", "anjani.maulaya@gmail.com", "082218297389", "Anjani123");
	
        userRepository.save(user);
        assertNotNull(userRepository.findAll());
    }
    
    @Test
    public void testShouldDeletUserById() {
    	User user = new User("Anjani", "Maulaya", "anjani.maulaya@gmail.com", "082218297389", "Anjani123");
        User savedUser = userRepository.save(user);
        userRepository.deleteById(savedUser.getId());
    }
	
}
