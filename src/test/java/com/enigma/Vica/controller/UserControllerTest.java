package com.enigma.Vica.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.enigma.Vica.entity.User;
import com.enigma.Vica.repository.UserRepository;
import com.enigma.Vica.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private UserRepository userRepository;
	
	private List<User> userList;
	
	@BeforeEach
	void setUp() {
		this.userList = new ArrayList<User>();
		this.userList.add(new User ("Anjani", "Maulaya", "anjani.maulaya@gmail.com", "082218297389", "Anjani123"));
		this.userList.add(new User ("Rama", "Tihara", "rama.tihara@gmail.com", "082218297389", "Anjani123"));
		this.userList.add(new User ("Bobby", "Hendra", "bobby.hendra@gmail.com", "081234567389", "Anjani123"));
	}
	
//	@Test
//	public void givenUser_whenGetUser_thenReturnJsonArray() throws Exception{
//		
//		User user = new User("Anjani", "Maulaya", "anjani.maulaya@gmail.com", "083452397389", "RamsTihara123");
//		userList = Arrays.asList(user);
//		
//		given(userService.getAllUser()).willReturn(userList);
//		
//		mvc.perform(get("/user")
//			.contentType(MediaType.APPLICATION_JSON))
//			.andExpect(status().isOk())
//			.andExpect(jsonPath())
//	
//	}
	
	@Test
	public void shouldFetchAllUsers() throws Exception{
		given(userService.getAllUser()).willReturn(userList);
		
		this.mvc.perform(get("/user"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.size()", is(userList.size())));
		
	}
	
		
}
